package com.example.serviciosmedicos.model.doctor;

import java.io.Serializable;

public class Doctor implements Serializable {
    private String professionalCard;
    private String speciality;
    private Float experience;
    private String consultingRoom;
    private boolean domicile;

    public Doctor(String professionalCard, String speciality, Float experience, String consultingRoom, boolean domicile) {
        this.professionalCard = professionalCard;
        this.speciality = speciality;
        this.experience = experience;
        this.consultingRoom = consultingRoom;
        this.domicile = domicile;
    }

    public String getProfessionalCard() {
        return professionalCard;
    }

    public void setProfessionalCard(String professionalCard) {
        this.professionalCard = professionalCard;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Float getExperience() {
        return experience;
    }

    public void setExperience(Float experience) {
        this.experience = experience;
    }

    public String getConsultingRoom() {
        return consultingRoom;
    }

    public void setConsultingRoom(String consultingRoom) {
        this.consultingRoom = consultingRoom;
    }

    public boolean isDomicile() {
        return domicile;
    }

    public void setDomicile(boolean domicile) {
        this.domicile = domicile;
    }
}
