package com.example.serviciosmedicos.model.appointment;

import java.util.Date;

public class MedicalAppointment {
    private String id;
    private String idPatient;
    private String idDoctor;
    private int state;
    private Date medicalAppointmentDate;
    private Double feeValue;
    private Date arrivalDate;
    private byte[] patientSignature;

    public MedicalAppointment(String id, String idPatient, String idDoctor, int state, Date medicalAppointmentDate, Double feeValue, Date arrivalDate, byte[] patientSignature) {
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
        this.arrivalDate = arrivalDate;
        this.patientSignature = patientSignature;
    }

    public MedicalAppointment(String id, String idPatient, String idDoctor, int state, Date medicalAppointmentDate, Double feeValue, Date arrivalDate) {
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
        this.arrivalDate = arrivalDate;
    }

    public MedicalAppointment(String id, String idPatient, String idDoctor, int state, Date medicalAppointmentDate, Double feeValue) {
        this.id = id;
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
    }

    public MedicalAppointment(String idPatient, String idDoctor, int state, Date medicalAppointmentDate, Double feeValue) {
        this.idPatient = idPatient;
        this.idDoctor = idDoctor;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
    }

    public MedicalAppointment(String idPatient, int state, Date medicalAppointmentDate, Double feeValue) {
        this.idPatient = idPatient;
        this.state = state;
        this.medicalAppointmentDate = medicalAppointmentDate;
        this.feeValue = feeValue;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getMedicalAppointmentDate() {
        return medicalAppointmentDate;
    }

    public void setMedicalAppointmentDate(Date medicalAppointmentDate) {
        this.medicalAppointmentDate = medicalAppointmentDate;
    }

    public Double getFeeValue() {
        return feeValue;
    }

    public void setFeeValue(Double feeValue) {
        this.feeValue = feeValue;
    }

    public byte[] getPatientSignature() {
        return patientSignature;
    }

    public void setPatientSignature(byte[] patientSignature) {
        this.patientSignature = patientSignature;
    }
}
