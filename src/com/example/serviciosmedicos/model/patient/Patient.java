package com.example.serviciosmedicos.model.patient;

import java.io.Serializable;
import java.util.Date;

public class Patient implements Serializable {

    private String id;
    private String name;
    private String lastName;
    private Date birthdate;
    private boolean medication;

    public Patient(String id, String name, String lastName, Date birthdate, boolean medication) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.medication = medication;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isMedication() {
        return medication;
    }

    public void setMedication(boolean medication) {
        this.medication = medication;
    }
}
