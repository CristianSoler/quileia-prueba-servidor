import com.example.serviciosmedicos.model.doctor.Doctor;
import com.example.serviciosmedicos.model.patient.Patient;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;

public class Main {

    public static void main(String[] args) {
        try {

            String ip;
            try {
                Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface iface = interfaces.nextElement();
                    if (iface.isLoopback() || !iface.isUp())
                        continue;

                    Enumeration<InetAddress> addresses = iface.getInetAddresses();
                    while(addresses.hasMoreElements()) {
                        InetAddress addr = addresses.nextElement();

                        if (addr instanceof Inet6Address) continue;

                        ip = addr.getHostAddress();
                        System.out.println(iface.getDisplayName() + " " + ip);
                    }
                }
            } catch (SocketException e) {
                throw new RuntimeException(e);
            }
            //Socket de servidor para esperar peticiones de la red
            ServerSocket serverSocket = new ServerSocket(5000);
            System.out.println("Servidor> Servidor iniciado");
            System.out.println("Servidor> En espera de cliente...");
            //Socket de cliente
            Socket clientSocket;
            while(true){
                clientSocket = serverSocket.accept();
                ObjectInputStream is = new ObjectInputStream(clientSocket.getInputStream());
                ArrayList<Object> d = (ArrayList<Object>) is.readObject();
                ArrayList<Doctor> doctor = (ArrayList<Doctor>) d.get(0);
                ArrayList<Patient> patient = (ArrayList<Patient>) d.get(1);

                for (int i=0; i<=doctor.size()-1;i++){
                    System.out.println(doctor.get(i).getConsultingRoom());
                }
                for (int i=0; i<=patient.size()-1;i++){
                    System.out.println(patient.get(i).getId());
                }
                clientSocket.close();
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
